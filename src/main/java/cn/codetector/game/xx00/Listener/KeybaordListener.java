package cn.codetector.game.xx00.Listener;


import cn.codetector.game.xx00.board.selectedBlock;
import cn.codetector.game.xx00.main.mainDisplay;
import org.lwjgl.input.Keyboard;

public class KeybaordListener {
    public static void FetchKeys() {
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                switch (Keyboard.getEventKey()) {
                    case Keyboard.KEY_UP:
                        selectedBlock.moveUp();
                        break;
                    case Keyboard.KEY_DOWN:
                        selectedBlock.moveDown();
                        break;
                    case Keyboard.KEY_LEFT:
                        selectedBlock.moveLeft();
                        break;
                    case Keyboard.KEY_RIGHT:
                        selectedBlock.moveRight();
                        break;
                    case Keyboard.KEY_SPACE:
                        mainDisplay.sharedInstance().gameboard.addChecker();
                        break;
                    case Keyboard.KEY_R:
                        mainDisplay.sharedInstance().gameboard.ResetBoard();
                        break;
                }
            }
        }
    }
}
