package cn.codetector.game.xx00.board;

public class selectedBlock {
    private static int selectedBlock = 4;

    public static int getSelectedBlock() {
        return selectedBlock;
    }

    public static void setSelectedBlock(int block) {
        selectedBlock = block;
    }

    public static void moveUp() {
        if (selectedBlock < 6) {
            selectedBlock += 3;
        }
    }

    public static void moveDown() {
        if (selectedBlock > 2) {
            selectedBlock -= 3;
        }
    }

    public static void moveRight() {
        if (selectedBlock < 8) {
            selectedBlock++;
        }
    }

    public static void moveLeft() {
        if (selectedBlock > 0)
            selectedBlock--;
    }
}
