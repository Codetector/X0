package cn.codetector.game.xx00.board;

import cn.codetector.util.GLutil.BasicShape.ShapeBox;
import cn.codetector.util.GLutil.BasicShape.ShapeRoundDot;
import cn.codetector.util.Graph.Color.Color3;
import cn.codetector.util.Graph.Vector.Vec2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Gameboard {
    private final Logger logger = LogManager.getLogger(this.getClass().getSimpleName());
    private final int color1 = 0xF0F0F0;
    private final int color2 = 0x000000;
    private List<ShapeBox> boxes = new ArrayList<>();
    private int[] dots = new int[9];
    private int Player = 1;

    /**
     * @param size    The size of the board
     * @param boxSize Size of each box
     */
    public Gameboard(Vec2 size, Vec2 boxSize) {
        this.ResetBoard();
        for (int r = 0; r < size.GetX(); r++) {
            for (int c = 0; c < size.GetY(); c++) {
                int color;
                if (r % 2 == 0) {
                    color = color2;
                    if (c % 2 == 0) {
                        color = color1;
                    }
                } else {
                    color = color1;
                    if (c % 2 == 0) {
                        color = color2;
                    }
                }
                ShapeBox box = new ShapeBox(new Vec2(c * boxSize.X, r * boxSize.Y), boxSize, color, "");
                boxes.add(box);
            }
        }
    }

    public void addChecker() {
        if (this.dots[selectedBlock.getSelectedBlock()] == 0) {
            this.dots[selectedBlock.getSelectedBlock()] = this.Player;
            if (this.Player == 1) {
                this.Player = 2;
            } else {
                this.Player = 1;
            }
            this.Judge();
        }
    }

    public void Judge() {
        if (this.judge(dots, 1)) {
            System.out.println("Player 1(Red Dots) WIN!");
            JOptionPane.showMessageDialog(null, "Player 1(RED) WIN!");
            this.ResetBoard();
        }
        if (this.judge(dots, 2)) {
            System.out.println("Player 2(Green Dots) WIN!");
            JOptionPane.showMessageDialog(null, "Player 2(GREEN) WIN!");
            this.ResetBoard();
        }
        if (this.checkFull()) {
            System.out.println("DRAW");
            JOptionPane.showMessageDialog(null, "DRAW");
            this.ResetBoard();
        }
    }

    public boolean checkFull() {
        boolean full = true;
        for (int i : this.dots) {
            if (i == 0)
                full = false;
        }
        return full;
    }

    public boolean judge(int[] boxes, int forid) {
        boolean win = false;
        //Check for row win
        for (int i = 0; i < 3; i++) {
            if (boxes[i] == forid) {
                if ((boxes[i + 3] == forid && boxes[i + 6] == forid)) {
                    win = true;
                }
            }
        }
        for (int i = 0; i < 9; i += 3) {
            if (boxes[i] == forid) {
                logger.trace("I=" + i);
                if ((boxes[i + 1] == forid && boxes[i + 2] == forid)) {
                    win = true;
                }
            }
        }
        if ((boxes[0] == forid && boxes[4] == forid && boxes[8] == forid) || (boxes[2] == forid && boxes[4] == forid && boxes[6] == forid)) {
            win = true;
        }

        return win;
    }

    public void ResetBoard() {
        this.Player = 1;
        for (int i = 0; i < dots.length; i++) {
            dots[i] = 0;
        }
    }

    public void draw() {
        int i = 0;
        for (ShapeBox box : this.boxes) {

            if (i == selectedBlock.getSelectedBlock()) {
                if (this.Player == 1) {
                    box.setSelectorColor(new Color3(0xFF0000));
                } else {
                    box.setSelectorColor(new Color3(0x00FF00));
                }
                box.setSelected(true);
            } else {
                box.setSelected(false);
            }
            box.draw();
            if (this.dots[i] == 1) {
                new ShapeRoundDot(new Vec2(box.Position.X + 80, box.Position.Y + 80), 80).Draw(new Color3(0xFF0000));
            } else if (this.dots[i] == 2) {
                new ShapeRoundDot(new Vec2(box.Position.X + 80, box.Position.Y + 80), 80).Draw(new Color3(0x00FF00));
            }
            i++;
        }
    }

}
