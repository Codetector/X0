package cn.codetector.game.xx00.main;

import java.io.File;
import java.io.IOException;

public class GameMain {
    public static void main(String args[]) {
        File lwjgl = new File("./lib/lwjgl");
        String path = "";
        try {
            path = lwjgl.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.setProperty("org.lwjgl.librarypath", path);
        mainDisplay.sharedInstance().init();
    }
}