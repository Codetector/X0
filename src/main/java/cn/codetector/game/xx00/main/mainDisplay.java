package cn.codetector.game.xx00.main;

import cn.codetector.game.xx00.Listener.KeybaordListener;
import cn.codetector.game.xx00.board.Gameboard;
import cn.codetector.util.Graph.Vector.Vec2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import static org.lwjgl.opengl.GL11.*;

public class mainDisplay {
    private static mainDisplay instance = new mainDisplay();
    private final Logger logger = LogManager.getLogger(this.getClass().getSimpleName());
    public Gameboard gameboard = new Gameboard(new Vec2(3, 3), new Vec2(160, 160));
    private boolean isRunning = true;

    public static mainDisplay sharedInstance() {
        return instance;
    }

    public void init() {
        try {
            Display.setDisplayMode(new DisplayMode(480, 480));
            Display.create();

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(0, 480, 0, 480, 1, -1);
            glMatrixMode(GL_MODELVIEW);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        } catch (LWJGLException e) {
            e.printStackTrace();
        }
        this.run();
    }

    public void run() {
        while (this.isRunning && !Display.isCloseRequested()) {
            KeybaordListener.FetchKeys();
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            this.render();
            Display.sync(60);
            Display.update();
        }
        Display.destroy();
        System.exit(0);
    }

    private void render() {
        this.gameboard.draw();
    }
}
